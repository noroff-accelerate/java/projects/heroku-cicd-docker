package no.accelerate.deploymentconfigdemo.users.models.dtos;

import lombok.Data;

@Data
public class AppUserDTO {
    private int id;
    private String username;
    private String email;
}
