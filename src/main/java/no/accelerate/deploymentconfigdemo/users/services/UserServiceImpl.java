package no.accelerate.deploymentconfigdemo.users.services;

import no.accelerate.deploymentconfigdemo.users.exceptions.UserNotFoundException;
import no.accelerate.deploymentconfigdemo.users.models.AppUser;
import no.accelerate.deploymentconfigdemo.users.repositories.UserRepository;
import org.springframework.stereotype.Service;

import java.util.Collection;

@Service
public class UserServiceImpl implements UserService {
    private final UserRepository userRepository;

    public UserServiceImpl(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public Collection<AppUser> findAll() {
        return userRepository.findAll();
    }

    @Override
    public AppUser findById(int id) {
        return userRepository.findById(id)
                .orElseThrow(() -> new UserNotFoundException("User does not exist with that ID"));
    }

    @Override
    public int add(AppUser user) {
        return (userRepository.save(user)).getId();
    }

    @Override
    public void update(AppUser user) {
        userRepository.save(user);
    }

    @Override
    public void delete(int id) {
        userRepository.deleteById(id);
    }
}
