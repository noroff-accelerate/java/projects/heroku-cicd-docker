package no.accelerate.deploymentconfigdemo.users.mappers;

import no.accelerate.deploymentconfigdemo.users.models.AppUser;
import no.accelerate.deploymentconfigdemo.users.models.dtos.AppUserDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import java.util.Collection;

@Mapper(componentModel = "spring")
public interface UserMapper {
    AppUserDTO userToUserDto(AppUser user);
    AppUser userDtoToUser(AppUserDTO user);
    Collection<AppUserDTO> userToUserDto(Collection<AppUser> user);
}
